# How to write VancedAdBlock filters
## Similar with `javascript queryselector` and `css selector` but more easier

<blockquote> Light, pure, with self-contained, native executables with no external dependencies and minimal overhead<br>
Designed for VancedAdBlock by LFJ user scripts. You must [install lfj user scripts](//lfj.io) first.</blockquote>


This guide is designed to help you write and maintain your own VancedAdBlock filters. Creating your own filters gives you more control over the things you want to see and don’t want to see on the websites that you visit, including ads, images, requests, and scripts.

## Simple blocking rules
#### Domain selector

<ul>
<li>Domain selector must outside of curly bracket symbol <code>{}</code>
	<ul><li>example <code>domain1.com{}</code></li></ul></li>
<li> select mutiple domain should be separated with a comma <code>,</code>
	<ul><li>example <code>domain1.com,domain2.com{}</code></li></ul></li>
<li> To select all subdomains, you can add a period <code>.</code> before the root domain name 
	<ul><li>example <code>.domain2.com{}</code> applies to <code>domain2.com</code> and all of its subdomains such as <code>www.domain2.com</code>, <code>sub.domain2.com</code>...</li></ul></li>
</ul>

#### Elements selector
Similar with `javascript queryselector` & `css selector`

<ul>
<li>(*) Use a dollar sign <code>$</code> to select a javascript src file name
	<ul><li>example <code>$this_js_name_file.js</code></li></ul></li>

<li>Use a dot sign <code>.</code> to select elements by class name.
	<ul><li>example <code>.col-sm-2</code></li></ul></li>

<li>Use a hash sign <code>#</code> to select id of element.
	<ul><li>example <code>#box_main</code></li></ul></li>

<li>Use a box brackets <code>[ ]</code> to select element by attribute.
	<ul><li>example <code>[type*=text]</code></li></ul></li>

<li>Without any sign to select tag name.
	<ul><li>example <code>div</code></li></ul></li>

<li>Use <code>A>B</code> will only select B that are direct children to A (that is, there are no other elements inbetween).
	<ul><li>example <code>div>p</code></li></ul></li>

<li>Use <code>A B</code> will select any B that are inside A, even if there are other elements between them.
	<ul><li>example <code>div a img</code></li></ul></li>
	
<li>You can also combine descendant combinators and child combinators, or mutiple elements selector separated with a comma <code>,</code>
	<ul><li>for example <code>body div.col-sm-2>a,#box_main img.ad_top</code></li></ul></li>


</ul>



#### Action list
VancedAdBlock support 4 different action for selected element, includes: `hide`, `transpatent`, `remove`, `outViewport`,and `click`. We also have `setVal`, `domTrace`,and `letVal`, but not release yet.
- `hide`: completely hide elements.
- `transpatent`: invisible elements (hidden hides the element, but it still takes up space in the layout)
- (*) `remove`: delete elements from the DOM.
- `outViewport`: hide an element but still allows screen readers (and similar technologies) to read the contents.
- (*) `click`: command for emulator a click to defined elements. (close an ad as a human 😂)

Example, you want hide class `.content_ad`, use `.content_ad|hide`

If you want more exact to define, you can use `body>div.left div.content_ad` to select any `div` having class `.content_ad`, located inside of `body>div.left`.

(*): The action use javascript to execute, i won't recommend you use it, but in some cases it can help to handle elements not hideable (by removing the parents element of them).

#### Modify `width`, `height`, `padding`, `margin`
After you remove the ads, seem we're alot of empty spaces, Vanced AdBlock also support to change basic size `width`,`height` `padding` and `margin` size to 0% or 100%, it helpful to stretch,or extend good element's to fill up any waste spaces.

- For padding use `p0` to `p100`
- For margin use `m0` to `m100`
- For margin and  padding use `mp0` to `mp100`
- For width use `w0` to `w100`
- For height use `h0` to `h100`
- For width and height use `wh0` to `wh100`

Example, you want to change width of class `.content_gallery` to `68%`,then use `.content_gallery|w68`

**Why have many action ?**
- Because some websites trace dom to detect and prevent changes of elements. Old ad blocker is not good enough to handle this


##### Not available yet
- `setVal`: Set value/content for elements, it can be change.
- `letVal`: Define value/content for elements, freezed these elements, cannot change or modify it by any way.
- `domTrace`: Remove whenever elements added by tracking changes of DOM.


#### Element selectors with action
- Selector and Action must separate with a pipe symbol `|`, left of the pipe select the elements, then do the action defined in the right of the pipe. For example use `found|kill`, it's mean when it `found` it will be `killed`.
- New line for every action. 
- No whitespace character outside selector, one selection syntax must start with one alphabet character and end with one alphabet character, you can use whitespace in selector for example `div.Aclass div#nID img.bClass`, brackets or dot character `[]` `()` `:` `.` for vanced selection. for advanced options, you can refer to the syntax `queryselector`. If you have many different elements, you can separate it with a comma <code>,</code>
- However, for element that use square brackets `[]` to  select by attribute. It must begin with a new line for each selections

#### Full example of rules `construct`
- for domain `example.com` and all subdomain, the action is `hide` for all `h1` elements, and `remove` for element `a` with attribute `href` matching `www`

<pre>.example.com{
h1|hide
a[href*=www]|remove
}</pre>
