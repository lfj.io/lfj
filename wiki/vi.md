# Làm thế nào để tạo một bộ lọc adBlock nâng cao
## Nó tương tự như `javascript queryselector` và `css selector` nhưng đã được tối giản

<blockquote> Nhanh, gọn, nhẹ, thực thi thuần tuý trên javascript, không cần bất kỳ phần nào bổ sung bên ngoài, và có thể dùng quy ước để chặn khi nội dung quảng cáo còn chưa tải xong<br>

VancedAdBlock được thiết kế dành riêng cho tập lệnh người dùng LFJ (userscript). Bạn cần phải cài đặt [LFJ user script](//lfj.io) trước.</blockquote>

Hướng dẫn này được thiết kế để giúp bạn viết và duy trì các bộ lọc VancedAdBlock của riêng mình. Việc tạo bộ lọc của riêng bạn cho phép bạn kiểm soát nhiều hơn những thứ bạn muốn xem và không muốn xem trên các trang web mà bạn truy cập, bao gồm quảng cáo, hình ảnh, yêu cầu và tập lệnh.

## Từ những quy ước căn bản
#### Chọn tên miền
Để bộ lọc có hiệu lực, bạn phải xác định tên miền mà nó sẽ hoạt động.

<ul>
<li>Tên miền phải nằm bên ngoài dấu ngoặc nhọn <code>{}</code>
	<ul><li>ví dụ<code>domain1.com{}</code></li></ul></li>
<li>Bạn có thể chọn nhiều tên miền liên tục, ngăn cách với nhau bởi dấu phẩy <code>,</code>
	<ul><li>ví dụ <code>domain1.com,domain2.com{}</code></li></ul></li>
<li>Để chọn tất cả tên miền phụ, bạn có thể thêm dấu chấm <code>.</code> vào trước tên miền gốc 
	<ul><li>ví dụ <code>.domain2.com{}</code> sẽ áp dụng cho <code>domain2.com</code> và tất cả tên miền con của nó như <code>www.domain2.com</code> , <code>sub.domain2.com</code>...</li></ul></li>
</ul>

#### Bộ chọn phần tử bên trong tên miền đó
Sau khi chọn tên miền, bên trong của mỗi trang web có rất nhiều phần tử (như player, hình ảnh, khung soạn thảo, popup...), các phần tử này nằm trong một DOM. Thông qua LFJ bạn có thể ẩn, xoá hoặc thay đổi kích thước của phần tử đó.

Nó khá là giống với cấu trúc `javascript queryselector` và `css selector`.

<ul>
<li>(*) Để xoá một source javascript, bạn dùng ký hiệu đô la <code>$</code> rồi nhập tên của tập tin .js đó	<ul><li>ví dụ <code>$this_js_name_file.js</code></li></ul></li>

<li>Sử dụng dấu chấm <code>.</code> đê chọn phần tử theo tên class.
	<ul><li>ví dụ <code>.col-sm-2</code></li></ul></li>

<li>Sử dụng dấu thăng<code>#</code> đê chọn phần tử theo tên id.
	<ul><li>ví dụ <code>#box_main</code></li></ul></li>

<li>Dùng dấu ngoặc vuông <code>[ ]</code> đê chọn phần tử theo một thuộc tính của nó.
	<ul><li>ví dụ <code>[type*=text]</code></li></ul></li>

<li>Không dùng kí hiệu nào ở trên để chọn phần tử theo tên của chính nó.
	<ul><li>Như <code>div</code>, hoặc <code>img</code></li></ul></li>

<li>Dùng ký hiệu mũi nhọn giữa 2 (hoặc hơn) phần tử được chọn, ví dụ <code>A>B</code> thì sẽ chọn phần tử <code>B</code> nằm ở trong phần tử <code>A</code> (không có phần tử nào khác liền kề, hoặc nằm giữa <code>A</code> và <code>B</code>).</li>
<li>Để một khoảng trắng giữa 2 (hoặc hơn) phần tử được chọn, ví dụ <code>A B</code> sẽ chọn phần tử <code>B</code> nằm đâu đó bất kỳ trong <code>A</code> (giữa <code>A</code> và <code>B</code> có thể có nhiều phần tử ở giữa)
	<ul><li>ví dụ <code>div a img</code></li></ul></li>


Bạn có thể kết hợp toàn bộ những cú pháp ở trên lại với nhau để chọn nhiều phần tử, hoặc xác định vị trí cụ thể của nó (để không xoá nhầm). Nếu bạn có nhiều phần tử khác nhau muốn thực hiện cùng một hành động với nó, thì phân cách những phần tử đó bằng dấu phẩy <code>,</code>
<ul><li>ví dụ <code>body div.col-sm-2>a,#box_main img.ad_top</code></li></ul>






#### Hành động
VancedAdBlock hỗ trợ 4 hành động khác nhau cho mỗi phần tử mà bạn đã chọn: `hide`, `transpatent`, `remove`, `outViewport`,and `click`. Chúng tôi cũng có `setVal`, `domTrace`,and `letVal`, nhưng nó chưa được phát hành chính thức.


- `hide`: ẩn hoàn toàn phần tử đã chọn.
- `transpatent`: làm cho nó trong suốt (tàng hình) vẫn tốn diện tích cho khu vực này.
- (*) `remove`: xoá khỏi DOM.
- `outViewport`: Làm cho nó biến mất khỏi tầm nhìn của bạn (nhưng vẫn hiện ra ở đâu đó để các công cụ hoặc máy đọc có thể nhận biết được nó).
- (*) `click`: giả lập nhấp chuột vào phần tử đó. (để bấm vào chỗ tắt quảng cáo chẳng hạn 😂)

Giả sử nếu bạn muốn ẩn phần tử có class tên là `content_ad`, chỉ cần dùng `.content_ad|hide`

Nếu bạn muốn định vị nó một cách chính xác, ví dụ như `body>div.left div.content_ad` nghĩa là chọn phần tử  `div` có class tên là `content_ad`, nằm ở trong `body>div.left`.

(*): Hành động có chứa dấu hoa thị ghi chú ở trên, sẽ sử dụng javascript để thực thi, tôi sẽ không khuyên bạn sử dụng nó, nhưng trong một số trường hợp, nó có thể giúp xử lý các phần tử không thể ẩn (bằng cách xóa phần tử cha của chúng).


#### Sửa `width`, `height`, `padding`, `margin`
Sau khi bạn xóa quảng cáo, có vẻ như chúng ta còn rất nhiều không gian trống, Vanced AdBlock cũng hỗ trợ thay đổi kích thước cơ bản `width`,` height` `padding` và kích thước` margin` từ 0% đến 100%, rất hữu ích khi bạn muốn phóng to, thu nhỏ, hoặc mở rộng các phần tử để lấp đầy bất kỳ khoảng trống lãng phí nào.

- Để thay đổi padding, bạn nhớ chữ `p` (viết tắt ký tự đầu của từ padding)
- Tương tự, thay đổi margin, bạn nhớ chữ `m`
- Dùng cả padding và margin thì sẽ là chữ `pm`
- Chiều rộng width sẽ là chữ `w`
- Chiều cao height sẽ là chữ `h`
- Dùng cả width và height thì sẽ là chữ `wh`

Tiếp theo bạn chỉ việc điền số vào sau chữ đó, ví dụ `p50` hoặc `p0`, `wh100` hoặc `wh0`

Ví dụ bạn muốn cho class `.content_gallery` hiện thị 80% kích thước của màn hình, chỉ cần dùng `.content_gallery|w80`


**Tại sao có nhiều hành động vậy ?**
- Bởi vì một số trang web theo dõi DOM để phát hiện và chống lại việc thay đổi các phần tử. Nếu bạn cố gắn sửa đổi một cái gì đó mà nó đã cấm, rất có thể nó sẽ phát hiện và đảo ngược thao tác bạn vừa làm.


##### Chưa được phát hành
- `setVal`: Đổi giá trị của một input hoặc phân tử hoặc các thông số khác. Giá trị này có thể được làm mới hoặc ghi đè.
- `letVal`: Đặt giá trị chặn đầu, và đóng băng nó lại, các giá trị xuất hiện ở sau nó, có cùng tên thì sẽ không thể ghi đè lên được
- `domTrace`: Loại bỏ mọi theo dõi dom trước đó, đồng thời ghi đè quyền theo dõi DOM để loại bỏ mọi DOM đến sau theo cú pháp lọc của bạn.


#### Quy tắc khi kết hợp `bộ chọn` và `hành động`
- bộ chọn và hành động phải phân tách bằng ký hiệu ống `|`, bên trái của ký hiệu ống chọn các phần tử, sau đó thực hiện hành động được xác định ở bên phải của ký hiệu ống. Ví dụ: sử dụng `tìmthấy|xoábỏ`, thì `tìmthấy` là bộ chọn, và `xoábỏ` là hành động.
- Mỗi hành động khác nhau phải bắt đầu bằng một dòng mới. 
- Không có khoảng trắng bên ngoài bộ chọn, một cú pháp lựa chọn phải bắt đầu bằng một ký tự và kết thúc bằng một ký tự, bạn có thể sử dụng khoảng trắng ở giữa bộ chọn, ví dụ `div.Aclass div#nID img.bClass`, các ký hiệu dấu ngoặc hoặc chấm `[]` `()` `:` `.` để chọn lựa nâng cao, bạn có thể tham khảo cú pháp `queryselector`. Nếu bạn có nhiều phần tử khác nhau, bạn có thể phân tách nó bằng dấu phấy <code>,</code>.
- Tuy nhiên, đối với các phần tử sử dụng dấu ngoặc vuông <code>[ ]</code> để lựa chọn theo thuộc tính, thì nó phải bắt đầu bằng một dòng riêng biệt cho mỗi phần tử riêng biệt.

#### `Cấu trúc` của một bộ lọc cơ bản:
- Ví dụ ngay bên dưới sẽ áp dụng đối với miền `example.com` và tất cả miền phụ như `www.example.com`, khi truy cập bạn sẽ ẩn đối với tất cả các phần tử `h1` và xoá các phần tử `a`có chứa `www` trong liên kết

<pre>.example.com{
h1|hide
a[href*=www]|remove
}</pre>
